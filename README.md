# Kühlenbier Tap Firmware


- [Sistema operacional](#sistema-operacional)
- [Criptografia](#criptografia)
- [Aplicação principal](#aplicacao-principal)
	- [Atualização](#atualizacao)


## Sistema Operacional

- Distribuição utilizada para a montagem do sistema:
	- [Minibian Jessie](https://sourceforge.net/projects/minibian/) - kernel v4.1.18
- Pacote de gerenciamento gráfico:
	- [Fluxbox](http://fluxbox.org/) - v1.3.5
- Python v2.7 (pacotes adicionais):
	- [PyQt4](https://pypi.python.org/pypi/PyQt4) - Interface gráfica
	- [pyqrcode](https://pypi.python.org/pypi/PyQRCode/1.2.1) - Gerar QR Code
	- [pypng](https://pypi.python.org/pypi/pypng) - Salvar imagem QR Code
	- [RPi.GPIO](https://pypi.python.org/pypi/RPi.GPIO/0.6.3) - Controle de pinos GPIO
	- [netifaces](https://pypi.python.org/pypi/netifaces/0.10.6) - Obter endereço MAC

A imagem do sistema `tap_firmware.img` possui duas partições:

| File system | Mount point |   Size   | Sectors (512 bytes) |                    |
|-------------|:------------|:--------:|:-------------------:|--------------------|
| `FAT16`     | /boot/      |  61.1 MB |     16 - 125055     | Partição de boot   |
| `EXT4`      | /           |  1.5 GB  |   125056 - 3198975  | Partição do sistema|

Todas as mensagens durante o boot foram desviadas para outro console, a fim de inibir seu aparecimento na tela. Além disso, uma tela de boot com o logo da kühlenbier foi instalada. A imagem se encontra em: `/etc/splash.png`, e o script que a inicializa se encontra em `/etc/init.d/aasplash`.

As portas USB são desativadas durante o processo de boot pelo script `/etc/nanoconf`. Tal script também cuida da criptografia e atualização automática.


## Criptografia

O script `/etc/nanoconf` é iniciado durante o boot. É um script compilado, portanto de código fechado. O conteúdo do script está no final desta seção e pode ser recompilado (com o [shc](https://github.com/neurobin/shc), por exemplo) caso houver necessidade de modificação. Todos os outputs dos comandos são revertidos para `/dev/null`, com o intuito de impedir qualquer vazamento de informação. Além de desativar as portas USB, ele também gerencia a criptografia do código fonte da aplicação principal da chopeira, por meio dos seguintes passos:

- A ID de hardware da Raspberry é adquirida e usada para gerar uma chave única.

- Um sistema de arquivos volátil é criado na memória RAM no caminho `~/.rd/`. Nele serão armazenados os arquivos descriptografados. Dessa forma, eles não ficam salvos no disco e são apenas acessíveis pelo sistema.

- Se o sistema está iniciando pela primeira vez, os arquivos da pasta `~/app` são atualizados com o comando `git pull`, compactados e criptografados com a chave única. O arquivo criptografado é salvo em disco e os arquivos não criptografados são enviados para o sistema temporário para serem executados. A pasta `~/app` deixa de existir no sistema, ficando apenas a versão criptografada no disco.

- Nas próximas vezes que o sistema inicia, o arquivo é descriptografado com a chave única, descompactado, atualizado, uma cópia dos arquivos é movida para o sistema temporário, e então compactado e criptografado novamente.

- Os arquivos da aplicação principal, portanto, apenas ficam na memória, o que impossibilita acesso externo ou por meio de leitura do cartão SD.

- A chave é única e exclusiva para a Raspberry na qual ela foi gerada. Portanto, se o cartão for inserido em outra Raspberry, a aplicação não será capaz de iniciar.

Segue o conteúdo do script `nanoconf`:

```sh
#!/bin/sh

# Desabilita o endereçamento USB e alimentação das portas
echo 0 > /sys/bus/usb/devices/usb1/authorized_default
/usr/share/X11/locale/en_US.UTF-8/XI19N_OBJS -a off -p 2345 >/dev/null 2>/dev/null

# Gera a chave com base no HID
hid=$(cat /proc/cpuinfo | grep Serial | cut -d ' ' -f 2)
hid_hash=$(echo -n $hid | sha1sum | awk '{print $1}')

# Cria partição na RAM
mount -t tmpfs -o size=50M tmpfs /root/.rd >/dev/null 2>/dev/null

if [ -e /root/.enc.aes ]; then
	cd /root/.rd >/dev/null 2>/dev/null
	openssl aes-256-cbc -d -salt -in /root/.enc.aes -out /root/.rd/.unenc.tar -k $hid_hash >/dev/null 2>/dev/null
	tar xf .unenc.tar >/dev/null 2>/dev/null
	rm .unenc.tar >/dev/null 2>/dev/null
	rm /root/.enc.aes >/dev/null 2>/dev/null
	cd app >/dev/null 2>/dev/null
	git pull >/dev/null 2>/dev/null
	cd .. >/dev/null 2>/dev/null
	tar cf .unenc.tar app >/dev/null 2>/dev/null
	openssl aes-256-cbc -salt -in /root/.rd/.unenc.tar -out /root/.enc.aes -k $hid_hash >/dev/null 2>/dev/null
	rm .unenc.tar >/dev/null 2>/dev/null
else
	cd /root/app >/dev/null 2>/dev/null
	git pull >/dev/null 2>/dev/null
	cd .. >/dev/null 2>/dev/null
	tar cf .unenc.tar app >/dev/null 2>/dev/null
	openssl aes-256-cbc -salt -in /root/.unenc.tar -out /root/.enc.aes -k $hid_hash >/dev/null 2>/dev/null
	rm /root/.unenc.tar >/dev/null 2>/dev/null
	mv /root/app /root/.rd/ >/dev/null 2>/dev/null
fi
```


## Aplicação Principal

A aplicação é toda escrita em `Python v2.7`. Todos os arquivos ficam dentro de uma mesma pasta `app/`. Neste diretório, há 3 pastas:

- `img/` - Contém os arquivos de imagem carregados pelo programa, como QR Code, GIFs, frames, imagem do chopp etc.

- `img_v/` - Contém os arquivos de imagem específicos para o layout vertical.

- `res/` - Contém as outras resources. No momento, apenas as fontes.

No diretório raiz, há dois arquivos: `init.py` e `main.py`.

- O arquivo `init.py` tem o intuito de executar comandos antes de iniciar o aplicativo principal, como, por exemplo, a instalação de uma dependência.
- O arquivo `main.py` é o programa em si. Seu funcionamento está explicado no código, por meio de comentários.

Os dois arquivos são iniciados em sequência pelo fluxbox, por meio do script `~/.fluxbox/startup`.

### Atualização

A atualização está sendo feita através de um repositório git. Para mudar o repositório, basta deletar a pasta `~/app/` na imagem do firmware e clonar o novo repositório, apenas lembrando que a pasta raiz precisa ser `~/app/`. A imagem precisará ser gravada novamente em todas as chopeiras.

