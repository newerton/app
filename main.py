# -*- coding: utf-8 -*-
import sys, time, datetime, math, png, pyqrcode, socket, os, json, signal, netifaces, urllib2
import RPi.GPIO as GPIO
from PyQt4 import QtGui, QtCore
from PyQt4.QtGui import QGraphicsOpacityEffect
from PyQt4.QtCore import QThread,QRect,QPropertyAnimation,QParallelAnimationGroup
from shutil import copyfile

# Diretório raiz da aplicação.
os.chdir(os.path.expanduser('~/.rd/app'))

# Endereço e porta para conexão.
address = ('api.tap.rtn.com.br',8090)

mac_ad = netifaces.ifaddresses('eth0')[netifaces.AF_LINK][0]['addr'].upper() # Adquire o endereço MAC.
id_msg = '{"identifier":"tap","identity":"'+mac_ad+'","private_token":"dxAHdxMvf2Cg76gnNiR"}' # Monta a string de identidade para enviar ao servidor.

# Variáveis globais.
tap_info = '' # Armazena informações da chopeira.
count = 0 # Armazena a contagem de pulsos do sensor.
mlTotal = 0 # Armazena a quantidade retirada, em mililitros.
tentativa = 0 # Tentativas mal sucedidas de conexão com o servidor.
old_lu = '' # Armazena a url do logo recebido em 'tap_info'.

# Gera QR Code.
if os.path.isfile('./img/qrcode.png'):
	os.remove('./img/qrcode.png')
qr = pyqrcode.create(mac_ad, error='Q', version=2, mode='binary')
qr.png('./img/qrcode.png', scale=4)

# Verifica se há o arquivo 'remaining'. Este armazena o restante de chopp em mililitros (real e servidor).
if not os.path.isfile('../../.tf/remaining'):
	f = open('../../.tf/remaining','w')
	f.write('0.0;0')
	f.close()

# Função para contar pulsos do sensor de vazão.
def count_pulses(channel):
	global count
	count += 1

# Configura os pinos GPIO.
s1_pin = 27 # Pino correspondente ao sensor de vazão.
s2_pin = 22 # Pino correspondente à abertura/fechamento da válvula.
GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.setup(s1_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(s2_pin, GPIO.OUT)
GPIO.output(s2_pin, GPIO.LOW)
GPIO.add_event_detect(s1_pin, GPIO.FALLING, callback=count_pulses)

# Função para alteração do logo da cerveja.
# Em caso de url nula ou algum erro no download, uma imagem padrão é carregada.
def changeLogo(url):
	global old_lu
	
	if url == "":
		copyfile('img/chopp_noimg.jpg', 'img/chopp.jpg')
	else:
		if url != old_lu:
			try:
				uopen = urllib2.urlopen(url)
				stream = uopen.read()
				file = open('img/chopp.jpg','w')
				file.write(stream)
				file.close()
				os.system('mogrify -resize 200x200 img/chopp.jpg')
				old_lu = url
			except:
				copyfile('img/chopp_noimg.jpg', 'img/chopp.jpg')

# Função para alteração da orientação do display.
# Carrega os arquivos já preparados e reinicia o sistema.
def changeOrientation(o):
	if o == 2:
		os.system('cp /usr/share/X11/xorg.conf.d/.h_45-evdev.conf /usr/share/X11/xorg.conf.d/45-evdev.conf')
		os.system('cp /etc/X11/xorg.conf.d/.h_99-calibration.conf /etc/X11/xorg.conf.d/99-calibration.conf')
		os.system('cp /boot/.h_config.txt /boot/config.txt')
	else:
		os.system('cp /usr/share/X11/xorg.conf.d/.v_45-evdev.conf /usr/share/X11/xorg.conf.d/45-evdev.conf')
		os.system('cp /etc/X11/xorg.conf.d/.v_99-calibration.conf /etc/X11/xorg.conf.d/99-calibration.conf')
		os.system('cp /boot/.v_config.txt /boot/config.txt')
	GPIO.cleanup()
	os.system('reboot')

# Classe para declaração de sinais. Estes carregam informações das Threads para suas classes de origem.
class Signal(QtCore.QObject):
	sigUnlock = QtCore.pyqtSignal()
	sigBeerUpdate = QtCore.pyqtSignal()
	sigInat = QtCore.pyqtSignal()
	sigVol = QtCore.pyqtSignal(str)
	sigCabInat = QtCore.pyqtSignal(str)
	sigCabVol = QtCore.pyqtSignal(str)
	sigError = QtCore.pyqtSignal()
	sigServer = QtCore.pyqtSignal(str)
	sigNoCredit = QtCore.pyqtSignal()
	sigPlayGif = QtCore.pyqtSignal()
	sigFreeVol = QtCore.pyqtSignal()
	sigFreeVolStop = QtCore.pyqtSignal()

# Thread que executa a primeira conexão com o servidor. Iniciada pelo Layout 'Info'.
# Também é chamada para reestabelecer a conexão, caso haja algum problema.
class FirstConnection(QThread):

	def __init__(self):
		QThread.__init__(self)
		self.signal = Signal()

	def __del__(self):
		self.quit()

	def run(self):
		global tap_info
		global s
		global rec_data
		global tentativa

		tentativa = 0
		toBreak = False

		while True:
			try:
				# Estabelece conexão. Timeout de 1 minuto.
				s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
				s.settimeout(5)
				s.connect(address)

				rec_data = s.recv(1024) # Recebe handshake: 'Identify yourself'.
				s.sendall(id_msg) # Envia mensagem de identificação.
				rec_data = s.recv(1024) # Recebe resposta.
				server_msg = '['+rec_data+']'
				server_msg = json.loads(server_msg) # Carrega JSON.
				event = server_msg[0]['event'] # Armazena 'event' recebido.

				# Se evento for 'welcome'
				if event == 'welcome':

					# Envia uma compra que não conseguiu ser enviada anteriormente.
					if os.path.isfile('../../.tf/u_purchase'):
						f = open('../../.tf/u_purchase','r')
						u_purchase = f.read()
						f.close()
						s.sendall(u_purchase)
						rec_data = s.recv(1024)
						rec_data = '['+rec_data+']'
						rec_data = json.loads(rec_data)
						event = rec_data[0]['event']
						if event == 'purchase_added' or event == 'report_withdraw':
							os.remove('../../.tf/u_purchase')

					s.sendall('{"command":"get_tap_info"}') # Envia comando para receber informações da chopeira.
					tap_info = s.recv(2048) # Recebe informações.
					tap_info = '['+tap_info+']'
					tap_info = json.loads(tap_info)
					event = tap_info[0]['event'] # Armazena 'event' recebido.
					tap_info = tap_info[0]['data'] # Armazena 'data' recebido.

					# Se 'event' for 'tap_info' e status = 1
					if event == 'tap_info' and str(tap_info['status']) == '1':
						url = tap_info['beer']['logo'].replace('\\','') # Armazena url do logo.
						o = int(tap_info['orientation']) # Armazena orientação do display.
						changeLogo(url) # Chama função para alterar logo.
						
						# Se for necessário mudança de orientação
						if o != sOrient:
							# Tenta sair de maneira limpa.
							try:
								s.sendall('quit')
								s.close()
							except:
								pass

							# Chama função que altera orientação e reinicia o sistema.
							changeOrientation(o)
						
						# Tudo em ordem. Faz o break do loop.
						toBreak = True
					
					# Caso contrário, informa que o status não é 1 e tenta conectar novamente.
					else:
						tentativa += 1
						error_msg = 'Status da chopeira = '+str(tap_info['status'])
						s.sendall('quit')
						self.signal.sigServer.emit(error_msg)
						s.close()
				
				# Caso 'event' não seja 'welcome', obtém a mensagem de erro recebida e tenta conectar novamente.
				else:
					error_msg = server_msg[0]['message']
					tentativa += 1

					# Se o erro for 'Tap not found', emite o sinal apropriado.
					if 'ap not found' in error_msg:
						s.sendall('quit')
						self.signal.sigServer.emit('tnf')
						s.close()

					# Caso seja outro erro, emite a mensagem de erro recebida.
					else:
						s.sendall('quit')
						self.signal.sigServer.emit(error_msg)
						s.close()
			
			# Em caso de outras falhas, emite sinal de erro correspondente e tenta conectar novamente.
			except Exception as e:
				self.signal.sigServer.emit('tnt')
				tentativa += 1
				s.close()

			# Em caso de sucesso, sai do loop e termina a Thread.
			if toBreak:
				self.signal.sigServer.emit('conectado')
				break

			# Dorme por 1 segundo antes de tentar novamente.
			time.sleep(1)

# Thread que mantém a conexão e escuta por mensagens do servidor. Iniciada pelo Layout 'Main'.
class KeepAlive(QThread):

	def __init__(self):
		QThread.__init__(self)
		self.signal = Signal()

	def __del__(self):
		self.quit()

	def run(self):
		global tap_info
		global rec_data
		global p_info

		toBreak = False

		while True:
			try:
				rec_data = s.recv(2048) # Espera por uma mensagem.
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']

				# Se 'event' for 'force_open_tap' ou 'force_close_tap', abre/fecha a válvula e emite sinal para mensagem na tela.
				if event == 'force_open_tap':
					self.signal.sigFreeVol.emit()
					toBreak = True

				# Se 'event' for 'open_tap_request', envia confirmação ao servidor, inicia o Layout de compra e sai do loop.
				elif event == 'open_tap_request':
					p_info = rec_data[0]['data'] # 'Purchase info'.

					# Verifica se o usuário tem créditos antes de confirmar a abertura da válvula.
					if float(p_info['user_credit']) > 0:
						s.sendall('{"command":"tap_opened_report"}')
						self.signal.sigUnlock.emit()
						toBreak = True
					else:
						self.signal.sigServer.emit('nocred')

				# Se 'event' for 'tap_info', atualiza as informações da chopeira, verificando logo, status e orientação.
				elif event == 'tap_info':
					tap_info = rec_data[0]['data']
					o = int(tap_info['orientation'])
					if str(tap_info['status']) == '1':
						url = tap_info['beer']['logo']
						changeLogo(url)
						if o != sOrient:
							try:
								s.sendall('quit')
								s.close()
							except:
								pass
							changeOrientation(o)
						self.signal.sigBeerUpdate.emit()
					else:
						s.close()
						self.signal.sigError.emit()
						toBreak = True
			
			# Em caso de timeout, envia um 'ping' ao servidor.
			# Caso haja erro, encerra o layout e volta para a Thread 'FirstConnection' para reestabelecer a conexão.
			except socket.timeout:
				try:
					s.sendall('ping')
					ans = s.recv(8)
				except Exception as e:
					s.close()
					self.signal.sigError.emit()
					toBreak = True
			# Em caso de outros erros, apenas encerra a Thread atual e volta para a 'FirstConnection'.
			except:
				s.close()
				self.signal.sigError.emit()
				toBreak = True
			if toBreak:
				break

# Thread para retirada de bebida pelo funcionário
class GetFreeVol(QThread):

	def __init__(self):
		QThread.__init__(self)
		self.signal = Signal()

	def __del__(self):
		self.quit()

	def run(self):

		global rec_data
		global count

		# Declaração de variáveis
		old = 0 # Contagem de pulsos de comparação
		count = 0 # Contagem de pulsos atual
		mlTotal = 0.0 # Volume total retirado

		preco = tap_info['beer']['price'] # Preço recebido da API.
		c_preco = float(preco)/1000 # Calcula preço por mililitro.

		preco_final = 0.0 # Armazena o preço final da compra.

		GPIO.output(s2_pin, GPIO.HIGH) # Abre a válvula

		# Enquanto não receber o sinal para fechar a torneira
		while True:

			# Caso haja atividade, calcula o preço e volume
			if count > old:
				mlTotal += (count-old)*0.367107
				preco_final = mlTotal*c_preco
				if mlTotal < 0:
					self.signal.sigVol.emit('0;0,00')
				else:
					self.signal.sigVol.emit(str('%.f' % mlTotal)+';'+str('{0:.2f}'.format(preco_final)).replace('.',','))
				old = count
	
			time.sleep(1)
		

# Thread de escuta da retirada do funcionário. Cancela a retirada em caso de mensagem apropriada do servidor.
class FreeVolConn(QThread):

	def __init__(self):
		QThread.__init__(self)
		self.signal = Signal()

	def __del__(self):
		self.quit()

	def run(self):

		global rec_data
		global count

		# Declaração de variáveis
		count = 0 # Contagem de pulsos atual
		mlTotal = 0.0 # Volume total retirado

		while True:
			try:
				rec_data = s.recv(1024)
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']

				if event == 'force_close_tap':
					p_info = rec_data[0]['data'] # 'Purchase info'.
					break
			except:
				pass

		# Informações recebidas da API.
		beer_id = str(tap_info['beer']['id'])
		partner_id = str(tap_info['partner_id'])
		user_id = str(p_info['userId'])
		preco = str(tap_info['beer']['price'])

		GPIO.output(s2_pin, GPIO.LOW) # Fecha a válvula

		# Função que envia as informações da compra. Caso haja erro, armazena as informações para enviar quando a conexão for reestabelecida.
		def sendInfo(volume):
			report_withdraw = '{"command":"report_withdraw","user_id":'+user_id+',"price":'+preco+',"amount":'+volume+',"beer_id":'+beer_id+',"partner_id":'+partner_id+'}'
			try:
				s.sendall(report_withdraw)
				rec_data = s.recv(1024)
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']
				self.signal.sigServer.emit(event)
				if event != 'report_withdraw':
					raise Exception('report not added')
			except:
				f = open('../../.tf/u_purchase','w')
				f.write(report_withdraw)
				f.close()

			### Calcula o restante de chopp no barril e atualiza o servidor.------------------------------
			db_remaining = float(tap_info['remaining'])
			capacity = float(tap_info['capacity'])

			f = open('../../.tf/remaining','r')
			remaining = f.read().split(';')
			if len(remaining) == 1:
				f_db = math.ceil(float(remaining[0]))
			else:
				f_db = float(remaining[1])
			f_remaining = float(remaining[0])
			f.close()

			if db_remaining == f_db:
				new_remaining = f_remaining-(mlTotal/1000)
			else:
				new_remaining = db_remaining-(mlTotal/1000)

			if new_remaining < 0:
				new_remaining = 0.0

			f = open('../../.tf/remaining','w')
			f.write(str(new_remaining)+';'+str(math.floor(new_remaining)))
			f.close()

			# Prepara e envia o comando.
			update_tap_info = '{"command":"update_tap_info","remaining":'+str(math.floor(new_remaining))+'}'
			self.signal.sigFreeVolStop.emit() # sinal de finalizar compra
			try:
				s.sendall(update_tap_info)
				rec_data = s.recv(1024)
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']
				if event != 'tap_info_updated':
					f = open('../../.tf/remaining','w')
					f.write(str(new_remaining)+';'+str(db_remaining))
					f.close()
			except:
				f = open('../../.tf/remaining','w')
				f.write(str(new_remaining)+';'+str(db_remaining))
				f.close()
			###-------------------------------------------------------------------------------------------

			#self.signal.sigFreeVolStop.emit() # sinal de finalizar compra

		mlTotal = count*0.367107

		if mlTotal < 0:
			mlTotal = 0.0
		volume = str('%.f' % mlTotal)
		sendInfo(volume)

		GPIO.output(s2_pin, GPIO.LOW) # Redundância.

		# Saída limpa, conforme documentação.
		s.sendall('quit')
		s.close()

# Thread de compra. Iniciada pelo Layout 'Second'.
class GetVolume(QThread):

	def __init__(self):
		QThread.__init__(self)
		self.signal = Signal()

	def __del__(self):
		self.quit()

	def run(self):

		global rec_data
		global count

		# Declaração de variáveis
		idle = 0 # Tempo de inatividade
		old = 0 # Contagem de pulsos de comparação
		count = 0 # Contagem de pulsos atual
		mlTotal = 0.0 # Volume total retirado

		preco = tap_info['beer']['price'] # Preço recebido da API.
		c_preco = float(preco)/1000 # Calcula preço por mililitro.

		# Informações recebidas da API.
		beer_id = str(tap_info['beer']['id'])
		partner_id = str(tap_info['partner_id'])
		user_id = str(p_info['user_id'])
		user_credit = float(p_info['user_credit'])

		preco_final = 0.0 # Armazena o preço final da compra.

		GPIO.output(s2_pin, GPIO.HIGH) # Abre a válvula

		# Enquanto o tempo de inatividade for menor que 3
		while idle < 2.9:
			# Se não houver retirada, soma o tempo de inatividade.
			if count == old or count == 1:
				# Se o cliente ainda não começou a retirada, tempo de inatividade é 10.
				if count == 0:
					idle += 0.3
				# Caso contrário, tempo de inatividade é 3 e o GIF de timeout é mostrado.
				else:
					if idle == 0:
						self.signal.sigPlayGif.emit()
					idle += 1
			
			# Caso haja atividade, calcula o preço e volume
			else:
				mlTotal += (count-old)*0.367107
				preco_final = mlTotal*c_preco
				if mlTotal < 0:
					self.signal.sigVol.emit('0;0,00')
				else:
					self.signal.sigVol.emit(str('%.f' % mlTotal)+';'+str('{0:.2f}'.format(preco_final)).replace('.',','))
				idle = 0
				old = count

			# Se o preço ultrapassar os créditos do usuário, quebra o loop.
			if preco_final > user_credit:	
				break	
			time.sleep(1)

		GPIO.output(s2_pin, GPIO.LOW) # Fecha a válvula.

		#self.signal.sigServer.emit('fim') # Sinal para mensagem de finalizar compra.

		# Função que envia as informações da compra. Caso haja erro, armazena as informações para enviar quando a conexão for reestabelecida.
		def sendInfo(volume,signal):
			add_purchase = '{"command":"add_purchase","mac":"'+mac_ad+'","user_id":'+user_id+',"price":'+str(preco)+',"amount":'+volume+',"beer_id":'+beer_id+',"partner_id":'+partner_id+'}'
			try:
				s.sendall(add_purchase)
				#self.signal.sigServer.emit("Aguardando resposta")
				rec_data = s.recv(1024)
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']
				#self.signal.sigServer.emit(event)
				if event != 'purchase_added':
					raise Exception('purchase not added')
			except:
				pass
				#f = open('../../.tf/u_purchase','w')
				#f.write(add_purchase)
				#f.close()

			### Calcula o restante de chopp no barril e atualiza o servidor.------------------------------
			### Obs.: Como o servidor apenas aceita números inteiros nesse campo (o que não faz sentido),
			### é necessário salvar manualmente a quantidade restante real em um arquivo
			### e ir atualizando o servidor com números inteiros conforme vão sendo atingidos.
			db_remaining = float(tap_info['remaining'])
			capacity = float(tap_info['capacity'])

			f = open('../../.tf/remaining','r')
			remaining = f.read().split(';')
			if len(remaining) == 1:
				f_db = math.ceil(float(remaining[0]))
			else:
				f_db = float(remaining[1])
			f_remaining = float(remaining[0])
			f.close()

			if db_remaining == f_db:
				new_remaining = f_remaining-(mlTotal/1000)
			else:
				new_remaining = db_remaining-(mlTotal/1000)

			if new_remaining < 0:
				new_remaining = 0.0

			f = open('../../.tf/remaining','w')
			f.write(str(new_remaining)+';'+str(math.floor(new_remaining)))
			f.close()

			# Prepara e envia o comando.
			update_tap_info = '{"command":"update_tap_info","remaining":'+str(math.floor(new_remaining))+'}'
			try:
				s.sendall(update_tap_info)
				self.signal.sigServer.emit("teste")
				rec_data = s.recv(1024)
				rec_data = '['+rec_data+']'
				rec_data = json.loads(rec_data)
				event = rec_data[0]['event']
				#self.signal.sigServer.emit(event)
				if event != 'tap_info_updated':
					f = open('../../.tf/remaining','w')
					f.write(str(new_remaining)+';'+str(db_remaining))
					f.close()
			except:
				f = open('../../.tf/remaining','w')
				f.write(str(new_remaining)+';'+str(db_remaining))
				f.close()
			###-------------------------------------------------------------------------------------------

			if signal == 'inat':
				self.signal.sigInat.emit()
			else:
				self.signal.sigNoCredit.emit()

		# Tendo em vista que o servidor não limita números menores que zero no campo 'user_credit', prepara e envia o comando apropriado.
		if preco_final > user_credit:
			volume = str((int((user_credit/c_preco)*10000)/float(10000)))
			sendInfo(volume,'nocred')
		else:
			if mlTotal < 0:
				mlTotal = 0.0
			volume = str('%.f' % mlTotal)
			sendInfo(volume,'inat')

		GPIO.output(s2_pin, GPIO.LOW) # Redundância.

		# Saída limpa, conforme documentação.
		s.sendall('quit')
		s.close()

# Layout principal.
class MainLayout(QtGui.QWidget):

	# Função que atualiza os dados da chopeira na tela.
	def updBeerInfo(self):

		#se o valor for null causa erro no u''.join
		def testForNull(a_field):
			if not a_field:
				return ''
			else:
				return a_field

		name = u''.join(testForNull(tap_info['beer']['name']))
		description = u''.join(testForNull(tap_info['beer']['description']))
		tipo = u''.join(testForNull(tap_info['beer']['type']))
		coloracao = u''.join(testForNull(tap_info['beer']['color']))
		alc_str = u''.join(str(('%f' % float(tap_info['beer']['alcoholic_strength'])).rstrip('0').rstrip('.').replace('.',',')))
		ibu = u''.join(str(('%f' % float(tap_info['beer']['ibu'])).rstrip('0').rstrip('.').replace('.',',')))
		origin = u''.join(testForNull(tap_info['beer']['origin']))
		price_ml = float(tap_info['beer']['price'])/10
		price_formated = u''.join(str(('%0.2f' % price_ml).replace('.',',')))
		price = u"R$ " + price_formated + u"/100ml"
		manufacturer = u''.join(testForNull(tap_info['manufacturer']))

		self.lblNome.setText(name)
		self.chopp_img.setStyleSheet("QWidget { \
		background: url('./img/chopp.jpg') no-repeat;\
		background-position: center center;\
		border-radius: 100px; }")
		self.lblDescr.setText(description)
		self.lblEstilo.setText(tipo)
		self.lblColor.setText(coloracao)
		self.lblTeor.setText(alc_str)
		self.lblIBU.setText(ibu)
		self.lblOrigin.setText(origin)
		self.lblPrice.setText(price)
		self.lblManufacturer.setText(manufacturer)

	# Proteção de tela com fundo preto. Apenas modifica as cores, posições e visibilidade dos itens.
	def blackSS(self):
		self.lblBlack.setStyleSheet("QWidget { background: black; color: white; }")
		self.lblBlack.show()

		self.setStyleSheet("QWidget { background: black; color: white; }")

		self.lblNome.move(20,10)
		self.lblNome.setAlignment(QtCore.Qt.AlignLeft)
		self.lblNome.setStyleSheet("QWidget { background: none; }")
		self.lblItens.hide()
		self.chopp_img.hide()
		self.lblDescricao.setStyleSheet("QWidget { color: white; }")
		self.lblDescricao.raise_()
		self.qr_img.setGeometry(z[29],20,115,115)
		self.logo.hide()
		self.lblEstilo.move(20,z[38])
		self.lblColor.move(20,z[42])
		self.lblPrice.move(20,z[96])
		self.lblManufacturer.move(20,z[100])
		if sOrient == 2:
			self.lblTeor.move(130,z[46])
		else:
			self.lblTeor.move(20,z[46])
		self.lblIBU.move(20,z[50])
		self.lblOrigin.setGeometry(20,z[58],z[53]+200,z[54])
		self.lblEstilo.setText(u'Estilo: '+self.lblEstilo.text())
		self.lblColor.setText(u'Coloração: '+self.lblColor.text())
		self.lblTeor.setText(u'Teor: '+self.lblTeor.text())
		self.lblIBU.setText(u'IBU: '+self.lblIBU.text())
		self.lblOrigin.setText(u'Origem: '+self.lblOrigin.text())
		self.lblPrice.setText(u'R$: '+self.lblPrice.text())
		self.lblManufacturer.setText(u'Fabricante: '+self.lblManufacturer.text())
		self.lblEstilo.adjustSize()
		self.lblColor.adjustSize()
		self.lblTeor.adjustSize()
		self.lblIBU.adjustSize()
		self.lblPrice.adjustSize()
		self.lblManufacturer.adjustSize()

	# Proteção de tela com fundo branco.
	def whiteSS(self):
		self.lblBlack.setStyleSheet("QWidget { background: white; color: black; }")
		
		self.setStyleSheet("QWidget { background: white; color: black; }")

		self.lblDescricao.setStyleSheet("QWidget { color: black; }")
		self.lblDescr.setStyleSheet("QWidget { color: black; }")
		self.lblEstilo.setStyleSheet("QWidget { color: black; }")
		self.lblColor.setStyleSheet("QWidget { color: black; }")
		self.lblTeor.setStyleSheet("QWidget { color: black; }")
		self.lblIBU.setStyleSheet("QWidget { color: black; }")
		self.lblOrigin.setStyleSheet("QWidget { color: black; }")
		self.lblPrice.setStyleSheet("QWidget { color: black; }")
		self.lblManufacturer.setStyleSheet("QWidget { color: black; }")
	
	# Retorna os itens às suas posições e cores originais.
	def quitSS(self):
		self.lblBlack.hide()
		self.lblItens.show()
		self.chopp_img.show()
		self.lblNome.move(z[7],z[8])
		self.lblNome.setStyleSheet("QWidget { background: url('"+z[0]+"/nome_bg.jpg'); }")
		if sOrient == 2:
			self.lblNome.setAlignment(QtCore.Qt.AlignVCenter|QtCore.Qt.AlignLeft)
		else:
			self.lblNome.setAlignment(QtCore.Qt.AlignVCenter|QtCore.Qt.AlignCenter)
		
		self.qr_img.setGeometry(z[29],z[30],115,115)
		self.setStyleSheet("QWidget { background: none; }")
		self.logo.show()
		self.lblDescricao.setStyleSheet("QWidget { color: #ffe063; }")
		self.lblDescr.setStyleSheet("QWidget { color: white; }")
		self.lblEstilo.setStyleSheet("QWidget { color: white; }")
		self.lblColor.setStyleSheet("QWidget { color: white; }")
		self.lblTeor.setStyleSheet("QWidget { color: white; }")
		self.lblIBU.setStyleSheet("QWidget { color: white; }")
		self.lblOrigin.setStyleSheet("QWidget { color: white; }")
		self.lblPrice.setStyleSheet("QWidget { color: white; }")
		self.lblManufacturer.setStyleSheet("QWidget { color: white; }")
		self.lblEstilo.move(z[37],z[38])
		self.lblColor.move(z[41],z[42])
		self.lblTeor.move(z[45],z[46])
		self.lblIBU.move(z[49],z[50])
		self.lblOrigin.setGeometry(z[57],z[58],z[53],z[54])
		self.lblPrice.move(z[95],z[96])
		self.lblManufacturer.move(z[99],z[100])
		self.lblEstilo.setText(u''+self.lblEstilo.text()[8:])
		self.lblColor.setText(u''+self.lblColor.text()[11:])
		self.lblTeor.setText(u''+self.lblTeor.text()[6:])
		self.lblIBU.setText(u''+self.lblIBU.text()[5:])
		self.lblOrigin.setText(u''+self.lblOrigin.text()[8:])
		self.lblPrice.setText(u''+self.lblPrice.text()[4:])
		self.lblManufacturer.setText(u''+self.lblManufacturer.text()[12:])
		self.lblEstilo.adjustSize()
		self.lblColor.adjustSize()
		self.lblTeor.adjustSize()
		self.lblIBU.adjustSize()
		self.lblPrice.adjustSize()
		self.lblManufacturer.adjustSize()

	
	def __init__(self, parent=None):
		super(MainLayout, self).__init__(parent)

		## Fundo da proteção de tela
		self.lblBlack = QtGui.QLabel(self)
		self.lblBlack.setGeometry(0,0,800,800)
		self.lblBlack.setStyleSheet("QWidget { background: black; color: white; }")
		self.lblBlack.hide()
		##----------

		## Nome
		self.lblNome = QtGui.QLabel(self)
		self.lblNome.setFont(QtGui.QFont('Libel Suit', 17, QtGui.QFont.Bold))
		if sOrient == 4:
			self.lblNome.setAlignment(QtCore.Qt.AlignCenter)
		self.lblNome.setGeometry(z[1],z[2],z[3],z[4])
		self.lblNome.setStyleSheet("QWidget { background: url('"+z[0]+"/nome_bg.jpg'); }")

		self.animNome = QPropertyAnimation(self.lblNome,"pos")
		self.animNome.setDuration(400)
		self.animNome.setStartValue(QtCore.QPointF(z[5],z[6]))
		self.animNome.setEndValue(QtCore.QPointF(z[7],z[8]))
		##-----------


		## Itens
		self.lblItens = QtGui.QLabel(self)
		self.lblItens.setPixmap(QtGui.QPixmap(z[0]+'/itens.jpg'))
		
		self.animItens = QPropertyAnimation(self.lblItens,"pos")
		self.animItens.setDuration(300)
		self.animItens.setStartValue(QtCore.QPointF(z[9],z[10]))
		self.animItens.setEndValue(QtCore.QPointF(z[11],z[12]))

		##-----------


		## Imagem do Chopp
		self.chopp_img = QtGui.QLabel(self)
		self.chopp_img.setPixmap(QtGui.QPixmap(z[0]+'/chopp_frame.png'))
		self.chopp_img.setGeometry(z[13],z[14],z[15],z[16])
		self.chopp_img.setScaledContents(True)
		self.chopp_img.setAutoFillBackground(True)

		self.animChopp = QPropertyAnimation(self.chopp_img,"pos")
		self.animChopp.setDuration(300)
		self.animChopp.setStartValue(QtCore.QPointF(z[17],z[18]))
		self.animChopp.setEndValue(QtCore.QPointF(z[19],z[20]))
		##------------

		## QR Code
		self.qr_img = QtGui.QLabel(self)
		self.qr_img.setPixmap(QtGui.QPixmap('./img/qrcode.png'))
		self.qr_img.setAlignment(QtCore.Qt.AlignCenter)
		self.qr_img.setGeometry(z[27],z[28],115,115)
		self.animQr_img = QPropertyAnimation(self.qr_img,"pos")
		self.animQr_img.setDuration(400)
		self.animQr_img.setStartValue(QtCore.QPointF(z[27],z[28]))
		self.animQr_img.setEndValue(QtCore.QPointF(z[29],z[30]))
		##------------

		## Descrição
		self.lblDescricao = QtGui.QLabel(self)
		self.lblDescricao.setFont(QtGui.QFont('Libel Suit', 14))
		self.lblDescricao.move(z[21],z[22])
		self.lblDescricao.setStyleSheet("QWidget { color: #ffe063; }")
		self.lblDescricao.setText(u'Descrição:')

		self.lblDescr = QtGui.QLabel(self)
		self.lblDescr.setFont(QtGui.QFont('Roboto', 12))
		self.lblDescr.setGeometry(z[23],z[24],z[25],z[26])
		self.lblDescr.setStyleSheet("QWidget { color: white; }")
		self.lblDescr.setAlignment(QtCore.Qt.AlignJustify)
		self.lblDescr.setWordWrap(True)

		effect = QtGui.QGraphicsOpacityEffect()
		effect.setOpacity(0)

		self.lblDescr.setGraphicsEffect(effect)

		self.animDescr = QPropertyAnimation(effect,"opacity")
		self.animDescr.setDuration(500)
		self.animDescr.setStartValue(0)
		self.animDescr.setEndValue(1)
		##------------

		## Logo Kühlenbier
		self.logo = QtGui.QLabel(self)
		self.logo.setPixmap(QtGui.QPixmap('./img/logo.png'))

		self.animLogo = QPropertyAnimation(self.logo,"pos")
		self.animLogo.setDuration(400)
		self.animLogo.setStartValue(QtCore.QPointF(z[31],z[32]))
		self.animLogo.setEndValue(QtCore.QPointF(z[33],z[34]))
		##------------


		## Itens:labels
		self.lblEstilo = QtGui.QLabel(self)
		self.lblEstilo.setFont(QtGui.QFont('Roboto', 14))
		self.lblEstilo.setStyleSheet("QWidget { color: white; }")
		self.lblEstilo.setGeometry(z[35],z[36],sSize.width()-z[37]-123,24)
		self.lblEstilo.setWordWrap(True)

		self.animEstilo = QPropertyAnimation(self.lblEstilo,"pos")
		self.animEstilo.setDuration(300)
		self.animEstilo.setStartValue(QtCore.QPointF(z[35],z[36]))
		self.animEstilo.setEndValue(QtCore.QPointF(z[37],z[38]))


		self.lblColor = QtGui.QLabel(self)
		self.lblColor.setFont(QtGui.QFont('Roboto', 14))
		self.lblColor.setStyleSheet("QWidget { color: white; }")
		self.lblColor.setGeometry(z[39],z[40],sSize.width()-z[41]-153,24)
		self.lblColor.setWordWrap(True)
		self.animColor = QPropertyAnimation(self.lblColor,"pos")
		self.animColor.setDuration(300)
		self.animColor.setStartValue(QtCore.QPointF(z[39],z[40]))
		self.animColor.setEndValue(QtCore.QPointF(z[41],z[42]))


		self.lblTeor = QtGui.QLabel(self)
		self.lblTeor.setFont(QtGui.QFont('Roboto', 14))
		self.lblTeor.setStyleSheet("QWidget { color: white; }")
		self.lblTeor.setGeometry(z[43],z[44],80,24)

		self.animTeor = QPropertyAnimation(self.lblTeor,"pos")
		self.animTeor.setDuration(300)
		self.animTeor.setStartValue(QtCore.QPointF(z[43],z[44]))
		self.animTeor.setEndValue(QtCore.QPointF(z[45],z[46]))


		self.lblIBU = QtGui.QLabel(self)
		self.lblIBU.setFont(QtGui.QFont('Roboto', 14))
		self.lblIBU.setStyleSheet("QWidget { color: white; }")
		self.lblIBU.setGeometry(z[47],z[48],80,24)

		self.animIBU = QPropertyAnimation(self.lblIBU,"pos")
		self.animIBU.setDuration(300)
		self.animIBU.setStartValue(QtCore.QPointF(z[47],z[48]))
		self.animIBU.setEndValue(QtCore.QPointF(z[49],z[50]))


		self.lblOrigin = QtGui.QLabel(self)
		self.lblOrigin.setFont(QtGui.QFont('Roboto', 14))
		self.lblOrigin.setStyleSheet("QWidget { color: white; }")
		self.lblOrigin.setWordWrap(True)
		self.lblOrigin.setAlignment(QtCore.Qt.AlignTop)
		self.lblOrigin.setGeometry(z[51],z[52],z[53],z[54])

		self.animOrigin = QPropertyAnimation(self.lblOrigin,"pos")
		self.animOrigin.setDuration(300)
		self.animOrigin.setStartValue(QtCore.QPointF(z[55],z[56]))
		self.animOrigin.setEndValue(QtCore.QPointF(z[57],z[58]))

		self.lblPrice = QtGui.QLabel(self)
		self.lblPrice.setFont(QtGui.QFont('Roboto', 14))
		self.lblPrice.setStyleSheet("QWidget { color: white; }")
		self.lblPrice.setGeometry(z[93],z[94],sSize.width()-z[95]-153,24)

		self.animPrice = QPropertyAnimation(self.lblPrice,"pos")
		self.animPrice.setDuration(300)
		self.animPrice.setStartValue(QtCore.QPointF(z[93],z[94]))
		self.animPrice.setEndValue(QtCore.QPointF(z[95],z[96]))

		self.lblManufacturer = QtGui.QLabel(self)
		self.lblManufacturer.setFont(QtGui.QFont('Roboto', 14))
		self.lblManufacturer.setStyleSheet("QWidget { color: white; }")
		self.lblManufacturer.setGeometry(z[97],z[98],sSize.width()-z[99]-153,24)
		self.lblManufacturer.setWordWrap(True)

		self.animManufacturer = QPropertyAnimation(self.lblManufacturer,"pos")
		self.animManufacturer.setDuration(300)
		self.animManufacturer.setStartValue(QtCore.QPointF(z[97],z[98]))
		self.animManufacturer.setEndValue(QtCore.QPointF(z[99],z[100]))

		##------------

		self.lblServerMsg = QtGui.QLabel(self)
		self.lblServerMsg.setFont(QtGui.QFont('Roboto', 14))
		self.lblServerMsg.setAlignment(QtCore.Qt.AlignCenter)
		self.lblServerMsg.setStyleSheet("QWidget { background: #3b2c27; border: 2px solid #ffe063; border-radius: 5px; color: white; }")
		self.lblServerMsg.setGeometry(z[91],z[92],300,100)
		self.lblServerMsg.hide()

		self.updBeerInfo()

		# Animações
		self.animGroup = QParallelAnimationGroup()
		self.animGroup.addAnimation(self.animNome)
		self.animGroup.addAnimation(self.animItens)
		self.animGroup.addAnimation(self.animEstilo)
		self.animGroup.addAnimation(self.animColor)
		self.animGroup.addAnimation(self.animTeor)
		self.animGroup.addAnimation(self.animIBU)
		self.animGroup.addAnimation(self.animOrigin)
		self.animGroup.addAnimation(self.animChopp)
		self.animGroup.addAnimation(self.animDescr)
		self.animGroup.addAnimation(self.animQr_img)
		self.animGroup.addAnimation(self.animLogo)
		self.animGroup.addAnimation(self.animPrice)
		self.animGroup.addAnimation(self.animManufacturer)
		self.animGroup.start()

	# Mostra na tela a mensagem recebida.
	def showMsg(self,event):
		self.lblServerMsg.show()
		if event == 'nocred':
			self.lblServerMsg.setText(u'Usuário sem créditos')
		elif event == 'o':
			self.lblServerMsg.setText(u'Válvula aberta')
		else:
			self.lblServerMsg.setText(u'Válvula fechada')
		QtCore.QTimer.singleShot(1000, self.lblServerMsg.hide)


# Layout de compra.
class SecondLayout(QtGui.QWidget):
	def __init__(self, parent=None):
		super(SecondLayout, self).__init__(parent)

		## Img.volume e lblvolume,preço
		self.lblVolimg = QtGui.QLabel(self)
		self.lblVolimg.setPixmap(QtGui.QPixmap('./img/purchase_info.jpg'))
				
		self.animVolimg = QPropertyAnimation(self.lblVolimg,"pos")
		self.animVolimg.setDuration(300)
		self.animVolimg.setStartValue(QtCore.QPointF(z[59],z[60]))
		self.animVolimg.setEndValue(QtCore.QPointF(z[61],z[62]))

		self.lblvolume = QtGui.QLabel(self)
		self.lblvolume.setFont(QtGui.QFont('Libel Suit', 70, QtGui.QFont.Bold))
		self.lblvolume.setGeometry(z[63],z[64],z[65],z[66])
		self.lblvolume.setText('0')
		self.lblvolume.setAlignment(QtCore.Qt.AlignRight)
		self.lblvolume.setStyleSheet("QWidget { color: #ffe063; background: none; }")

		self.animvolume = QPropertyAnimation(self.lblvolume,"pos")
		self.animvolume.setDuration(300)
		self.animvolume.setStartValue(QtCore.QPointF(z[67],z[68]))
		self.animvolume.setEndValue(QtCore.QPointF(z[69],z[70]))

		self.lblpreco = QtGui.QLabel(self)
		self.lblpreco.setFont(QtGui.QFont('Libel Suit', 40, QtGui.QFont.Bold))
		self.lblpreco.setGeometry(z[63],z[64],z[65],z[66]-30)
		self.lblpreco.setText('0,00')
		self.lblpreco.setAlignment(QtCore.Qt.AlignRight)
		self.lblpreco.setStyleSheet("QWidget { color: #ffe063; background: none; }")

		self.animpreco = QPropertyAnimation(self.lblpreco,"pos")
		self.animpreco.setDuration(300)
		self.animpreco.setStartValue(QtCore.QPointF(z[67],z[68]))
		self.animpreco.setEndValue(QtCore.QPointF(z[69],z[70]+150))
		##------------

		## Copo
		self.lblMug = QtGui.QLabel(self)
		self.lblMug.setPixmap(QtGui.QPixmap('./img/f_mug.png'))
		self.lblMug.setStyleSheet("QWidget { background: none; }")
		self.lblMug.setAlignment(QtCore.Qt.AlignBottom)
		self.lblMug.setGeometry(z[61]+25,z[62]+242,71,0)

		self.animMug = QPropertyAnimation(self.lblMug,"geometry")
		self.animMug.setDuration(970)
		##----


		## Liberado
		self.lblLiberado = QtGui.QLabel(self)
		self.lblLiberado.setPixmap(QtGui.QPixmap(z[0]+'/liberado.jpg'))
				
		self.animLiberado = QPropertyAnimation(self.lblLiberado,"pos")
		self.animLiberado.setDuration(300)
		self.animLiberado.setStartValue(QtCore.QPointF(z[71],z[72]))
		self.animLiberado.setEndValue(QtCore.QPointF(z[73],z[74]))
		##------------

		## Mensagem de finalização
		self.lblServerMsg = QtGui.QLabel(self)
		self.lblServerMsg.setFont(QtGui.QFont('Roboto', 20))
		self.lblServerMsg.setAlignment(QtCore.Qt.AlignCenter)
		self.lblServerMsg.setStyleSheet("QWidget { background: #3b2c27; border: 2px solid #ffe063; border-radius: 5px; color: white; }")
		self.lblServerMsg.setGeometry(z[91],z[92],300,100)
		self.lblServerMsg.hide()
		##------------

		## Finalizado
		self.lblfinalizar = QtGui.QLabel(self)
		self.lblfinalizar.setPixmap(QtGui.QPixmap('./img/finalizado.jpg'))
		self.lblfinalizar.move(z[75],z[76])
				
		effect2 = QtGui.QGraphicsOpacityEffect()
		effect2.setOpacity(0)

		self.lblfinalizar.setGraphicsEffect(effect2)

		self.animfinalizar = QPropertyAnimation(effect2,"opacity")
		self.animfinalizar.setDuration(400)
		self.animfinalizar.setStartValue(0)
		self.animfinalizar.setEndValue(1)
		##------------

		## Sem Créditos
		self.lblsemcred = QtGui.QLabel(self)
		self.lblsemcred.setPixmap(QtGui.QPixmap('./img/nocred.jpg'))
		self.lblsemcred.move(z[77],z[78])
				
		effect3 = QtGui.QGraphicsOpacityEffect()
		effect3.setOpacity(0)

		self.lblsemcred.setGraphicsEffect(effect3)

		self.animsemcredito = QPropertyAnimation(effect3,"opacity")
		self.animsemcredito.setDuration(400)
		self.animsemcredito.setStartValue(0)
		self.animsemcredito.setEndValue(1)
		##------------

		## Timer GIF
		self.gif10 = QtGui.QLabel(self)
		self.movie10 = QtGui.QMovie("./img/t10.gif")
		self.gif10.setMovie(self.movie10)
		self.gif10.setGeometry(z[88],z[89],64,64)
		self.movie10.start()

		self.gif3 = QtGui.QLabel(self)
		self.movie3 = QtGui.QMovie("./img/t3.gif")
		self.gif3.setMovie(self.movie3)
		self.gif3.setGeometry(z[88],z[89],64,64)
		self.gif3.hide()
		##----------

		self.animGroup2 = QParallelAnimationGroup()
		self.animGroup2.addAnimation(self.animLiberado)
		self.animGroup2.addAnimation(self.animVolimg)
		self.animGroup2.addAnimation(self.animvolume)
		self.animGroup2.addAnimation(self.animpreco)
		self.animGroup2.start()

	def showMsg(self,event):
		self.lblServerMsg.show()
		if event == 'fim':
			self.lblServerMsg.setText(u'Finalizando compra...')
		else:
			self.lblServerMsg.setText(event)

	def playGif(self):
		time.sleep(.32)
		self.gif3.show()
		self.movie3.start()

	# Atualiza as informações de volume e preço. Também cuida da animação do chopp.
	def updVolLbl(self,volume):
		self.gif3.hide()
		self.gif10.hide()
		volume,preco = volume.split(';')
		self.lblvolume.setText(volume)
		self.lblpreco.setText(preco)

		volume = int(volume)
		excess = volume/400
		volume = volume-excess*400
		param = float(volume)*0.5
		self.animMug.setStartValue(self.lblMug.frameGeometry())
		self.animMug.setEndValue(QtCore.QRect(z[61]+25,z[62]+242-param,71,param))
		self.animMug.start()

	# Funções para mostrar a imagem de compra finalizada ou usuário sem créditos.
	def updateFinalizarLabel(self,a):
		self.lblServerMsg.hide()
		self.animfinalizar.start()

	def updateSemCredito(self,a):
		self.lblServerMsg.hide()
		self.animsemcredito.start()

	def hideGif(self,a):
		self.gif3.hide()

# Layout de informação de conexão. Primeiro layout a ser carregado. Também é carregado quando há problemas com a conexão.
class InfoLayout(QtGui.QWidget):

	def __init__(self, parent=None):
		super(InfoLayout, self).__init__(parent)

		self.lblTitulo = QtGui.QLabel(self)
		self.lblTitulo.setFont(QtGui.QFont('Roboto', 22))
		self.lblTitulo.setStyleSheet("QWidget { color: white; }")
		self.lblTitulo.setGeometry(20,20,330,40)
		
		self.lblInfo = QtGui.QLabel(self)
		self.lblInfo.setFont(QtGui.QFont('Roboto', 20))
		self.lblInfo.setStyleSheet("QWidget { color: white; }")
		self.lblInfo.setAlignment(QtCore.Qt.AlignTop)
		self.lblInfo.setWordWrap(True)
		self.lblInfo.setGeometry(z[84],z[85],z[86],z[87])


	def populate(self):
		self.lblTitulo.setText(u'Conexão com o servidor:')
		
	def updLblInfo(self,txt):
		self.lblInfo.setText(txt)


# Classe principal da GUI da aplicação.
class MainGUI(QtGui.QMainWindow):

	def __init__(self, parent=None):
		super(MainGUI, self).__init__(parent)

		# Define cor de fundo e fontes a serem carregadas.
		self.setStyleSheet("QWidget { background: #3b2c27; } ")
		QtGui.QFontDatabase.addApplicationFont('./res/libel-suit-rg.ttf')
		QtGui.QFontDatabase.addApplicationFont('./res/Roboto-Regular.ttf')

		self.startInfoLayout()

	# Função que inicia o Layout de compra.
	def startSecondLayout(self):
		self.SLayout = SecondLayout(self)
		self.setCentralWidget(self.SLayout)
		
		def compraFinalizada():
			self.SLayout.updateFinalizarLabel(self)
			self.getvolume.terminate()
			QtCore.QTimer.singleShot(2000, self.startInfoLayout)

		def semCredito():
			self.SLayout.updateSemCredito(self)
			self.getvolume.terminate()
			QtCore.QTimer.singleShot(2000, self.startInfoLayout)

		# Inicia a Thread para captura dos dados do sensor e envio de informações.
		# O sinais ativam as funções correspondentes (compra finalizada, usuário sem créditos, atualização dos valores de volume e preço e o gif de timeout).
		self.getvolume = GetVolume()
		self.getvolume.start()
		self.getvolume.signal.sigInat.connect(compraFinalizada)
		self.getvolume.signal.sigNoCredit.connect(semCredito)
		self.getvolume.signal.sigServer.connect(self.SLayout.showMsg)
		self.getvolume.signal.sigVol.connect(self.SLayout.updVolLbl)
		self.getvolume.signal.sigPlayGif.connect(self.SLayout.playGif)
		
		self.showFullScreen()

	# Função que inicia o Layout retirada de bebida pelo funcionário.
	def startFreeVolLayout(self):
		self.SLayout = SecondLayout(self)
		self.setCentralWidget(self.SLayout)
		self.SLayout.hideGif(self)
		
		def compraFinalizada():
			self.SLayout.updateFinalizarLabel(self)
			QtCore.QTimer.singleShot(3000, startInfo)

		def startInfo():
			GPIO.output(s2_pin, GPIO.LOW) # Fecha a válvula
			self.freevolconn.terminate()
			self.getfreevolume.terminate()
			self.startInfoLayout()

		# Inicia a Thread para captura dos dados do sensor e envio de informações.
		# O sinais ativam as funções correspondentes (compra finalizada, usuário sem créditos, atualização dos valores de volume e preço e o gif de timeout).
		self.getfreevolume = GetFreeVol()
		self.getfreevolume.start()
		self.getfreevolume.signal.sigServer.connect(self.SLayout.showMsg)
		self.getfreevolume.signal.sigVol.connect(self.SLayout.updVolLbl)

		# Inicia a Thread de escuta ao servidor.
		self.freevolconn = FreeVolConn()
		self.freevolconn.start()
		self.freevolconn.signal.sigFreeVolStop.connect(compraFinalizada)
		
		self.showFullScreen()

	# Função que inicia o Layout principal.
	def startMainLayout(self):
		self.MLayout = MainLayout(self)
		self.setCentralWidget(self.MLayout)

		# Define os timers para a proteção de tela
		ssTimer = QtCore.QTimer()
		ssTimer1 = QtCore.QTimer()
		ssTimer1.setSingleShot(True)
		ssTimer2 = QtCore.QTimer()
		ssTimer2.setSingleShot(True)
		ssTimerQuit = QtCore.QTimer()
		ssTimerQuit.setSingleShot(True)
		rebootCount = 0 # Contagem de quantas vezes entrou na proteção de tela, após 3 vezes a chopeira é reiniciada

		def enterSL():
			rebootCount = 0
			ssTimer.stop()
			ssTimer1.stop()
			ssTimer2.stop()
			ssTimerQuit.stop()
			self.ka.terminate()
			self.startSecondLayout()

		def enterFreeVolLayout():
			rebootCount = 0
			ssTimer.stop()
			ssTimer1.stop()
			ssTimer2.stop()
			ssTimerQuit.stop()
			self.ka.terminate()
			self.startFreeVolLayout()

		def startSS():
			if rebootCount == 2:
				rebootTap()
			else:
				rebootCount += 1
				ssTimer1.start(50)
				ssTimer2.start(1000*30) # 30 segundos
				ssTimerQuit.start(1000*60) # +30 segundos

		def rebootTap():
			s.close()
			self.ka.terminate()
			self.startInfoLayout()

		ssTimer.timeout.connect(startSS)
		ssTimer1.timeout.connect(self.MLayout.blackSS)
		ssTimer2.timeout.connect(self.MLayout.whiteSS)
		ssTimerQuit.timeout.connect(self.MLayout.quitSS)
		ssTimer.start(1000*60*60) # 60 minutos

		# Inicia a Thread de conexão e escuta ao servidor e seus respectivos sinais.
		self.ka = KeepAlive()
		self.ka.start()
		self.ka.signal.sigUnlock.connect(enterSL)
		self.ka.signal.sigFreeVol.connect(enterFreeVolLayout)
		self.ka.signal.sigError.connect(self.startInfoLayout)
		self.ka.signal.sigServer.connect(self.MLayout.showMsg)
		self.ka.signal.sigBeerUpdate.connect(self.MLayout.updBeerInfo)

		self.showFullScreen()
		

	# Inicia o Layout de informações de conexão.
	def startInfoLayout(self):
		global tentativa
		global mac_ad

		self.ILayout = InfoLayout(self)
		self.setCentralWidget(self.ILayout)

		# Gerencia os diferentes sinais de erro e atualiza as informações na tela.
		def server_signal(content):
			if content == 'conectado':
				self.startMainLayout()
			elif content == 'tnf':
				msg = 'Conectado ao servidor.\n\nTentativa # '+str(tentativa)+'\nChopeira não encontrada no banco.\nTentando novamente...\n\n\nEndereço MAC desta chopeira: '+mac_ad
				try:
					self.ILayout.populate()
					self.ILayout.updLblInfo(msg.decode('utf-8'))
				except:
					pass
			elif content == 'tnt':
				msg = 'Tentativa # '+str(tentativa)+' - Não conectado\nTentando novamente...'
				try:
					self.ILayout.populate()
					self.ILayout.updLblInfo(msg.decode('utf-8'))
				except:
					pass
			else:
				try:
					self.ILayout.populate()
					self.ILayout.updLblInfo(u'Conectado ao servidor.\n\nTentativa # '+str(tentativa)+'\nMensagem de erro: '+content+'\n\nTentando novamente...')
				except:
					pass
			if 'Status da chopeira' in content:				
				msg = 'Conectado ao servidor.\n\nTentativa # '+str(tentativa)+'\n'+content+'\n\nTentando novamente...'
				try:
					self.ILayout.populate()
					self.ILayout.updLblInfo(msg.decode('utf-8'))
				except:
					pass

		# Inicia a Thread de estabelecimento de conexão.
		self.fconec = FirstConnection()
		self.fconec.start()
		self.fconec.signal.sigServer.connect(server_signal)

		self.showFullScreen()

	# Tecla Esc fecha a aplicação.
	def keyPressEvent(self, e):
		if e.key() == QtCore.Qt.Key_Escape:
			self.close()


# Função principal da aplicação.
def main():
	
	global z # Armazena as posições e dimensões dos elementos, conforme a orientação da tela.
	global sOrient # Armazena a orientação da tela.
	global sSize

	app = QtGui.QApplication(sys.argv)

	sSize = QtGui.QDesktopWidget().screenGeometry() # Obtém a geometria da tela.

	# Obtém o valor de orientação, conforme documentação.
	if sSize.width() > sSize.height():
		sOrient = 2
	else:
		sOrient = 4
	
	# A variável z é uma lista com todas as posições e dimensões dos elementos que precisam de ajuste quando a orientação da tela é alterada.
	if sOrient == 2:
		z = ['./img',280,0,447,53,-186,0,186,0,-100,65,120,65,0,0,200,200,-200,0,0,0,20,350,20,375,760,96,800,250,664,250,800,100,
			660,100,800,128,255,128,800,175,225,175,800,232,170,232,800,232,303,232,800,276,450,60,800,276,170,276,180,-300,180,130,
			300,-210,200,100,450,-210,415,125,100,-60,100,0,180,130,180,130,20,100,760,380,580,20,100,760,380,716,396,20,250,190,
			800,320,170,320,800,73,260,73]
	else:
		z = ['./img_v',-200,0,480,45,-200,0,0,0,-50,300,18,247,-200,32,200,200,-200,32,140,32,20,600,20,625,440,160,480,500,345,500,480,
			325,342,325,-50,307,75,307,-50,358,75,358,-50,407,75,407,-50,458,75,458,-50,508,230,60,-50,508,75,508,90,-300,20,250,
			140,-210,200,110,140,-210,260,245,0,-60,0,0,20,250,20,250,20,120,440,400,260,20,100,440,600,396,716,700,90,350,
			-50,557,75,557,-50,262,75,262]

	w = MainGUI()
	sys.exit(app.exec_())

if __name__ == '__main__':

	main()