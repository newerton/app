# -*- coding: utf-8 -*-

# # O arquivo está todo comentado, pois ainda não está sendo utilizado.

# import sys, time, datetime, os, signal
# from PyQt4 import QtGui, QtCore
# from PyQt4.QtCore import QThread
# from shutil import copyfile

# # altera diretório atual o apropriado. Para casos de caminhos relativos.
# os.chdir(os.path.expanduser('~/.rd/app'))

# # Thread que executa as ações necessárias
# class Info(QThread):

# 	def __init__(self):
# 		QThread.__init__(self)

# 	def __del__(self):
# 		self.wait()

# 	# Lógica principal
# 	def run(self):

# 		time.sleep(.15)

# 		# Fecha a aplicação ao finalizar
# 		QtGui.QApplication.quit()

# # Classe principal - cria a GUI
# class MainGUI(QtGui.QMainWindow):

# 	def __init__(self, parent=None):
# 		super(MainGUI, self).__init__(parent)

# 		# Armazena as dimensões da tela
# 		size = QtGui.QDesktopWidget().screenGeometry()

# 		# Configura cor do fundo e carrega a fonte necessária
# 		self.setStyleSheet("QWidget { background: black; } ")
# 		QtGui.QFontDatabase.addApplicationFont('./res/Roboto-Regular.ttf')

# 		# Configura label de informações. Pode ser atualizado através de um 'signal'.
# 		self.lblInfo = QtGui.QLabel(self)
# 		self.lblInfo.setGeometry(0,0,size.width(),size.height())
# 		self.lblInfo.setFont(QtGui.QFont('Roboto', 24))
# 		self.lblInfo.setStyleSheet("QWidget { color: white; }")
# 		self.lblInfo.setAlignment(QtCore.Qt.AlignCenter)
# 		self.lblInfo.setText('Verificando...')

# 		# Inicia a Thread de execução
# 		self.info = Info()
# 		self.info.start()

# 		# Mostra a GUI em tela cheia
# 		self.showFullScreen()

# # Execução da aplicação
# def main():
	
# 	app = QtGui.QApplication(sys.argv)
# 	w = MainGUI()
# 	sys.exit(app.exec_())

# if __name__ == '__main__':

# 	main()
